# -*- coding: utf-8 -*-
# @Time    : 2017/6/4 20:35
# @Author  : lileilei
# @File    : get_excel.py
import xlrd
from common.lognew  import LOG,logger
@logger('解析测试用例文件')
def datacel(filrpath):
    try:
        file=xlrd.open_workbook(filrpath)
        me=file.sheets()[0]
        nrows=me.nrows
        listid=[]
        listkey=[]
        listconeent=[]
        listurl=[]
        listfangshi=[]
        listqiwang=[]
        listname=[]
        listmodel=[]
        listheaders=[]
        listpid=[]
        listis_data=[]
        listsql=[]
        listziduan=[]
        listyican=[]
        listsave=[]
        list_test_url=[]
        for i in range(1,nrows):
            listid.append(me.cell(i,0).value)
            listkey.append(me.cell(i,2).value)
            listconeent.append(me.cell(i,3).value)
            listurl.append(me.cell(i,4).value)
            listname.append(me.cell(i,1).value)
            listfangshi.append((me.cell(i,5).value))
            listqiwang.append((me.cell(i,6).value))
            listmodel.append((me.cell(i,7).value))
            listheaders.append((me.cell(i,8).value) )
            listpid.append(me.cell(i,9).value)
            listis_data.append(me.cell(i,10).value)
            listsql.append(me.cell(i,11).value)
            listziduan.append(me.cell(i,12).value)
            listyican.append(me.cell(i,13).value)
            listsave.append(me.cell(i,14).value)
            list_test_url.append(me.cell(i,15).value)
        return listid,listkey,listconeent,listurl,listfangshi,\
               listqiwang,listname,listmodel,listheaders,\
               listpid,listis_data,listsql,listziduan,listyican,listsave,list_test_url
    except Exception as e:
        LOG.info('打开测试用例失败，原因是:%s'%e)
        return
@logger('生成数据驱动所用数据')
def makedata():
    import os
    path = os.getcwd() + '\\app\\upload\\case.xls'
    listid, listkey, listconeent, listurl, listfangshi,  listqiwang, listname, listmodel, listheaders, listpid, listis_data, listsql, listziduan, listyican, listsave,list_test_url=datacel(filrpath=path)
    make_data=[]
    for i in range(len(listid)):
        make_data.append({'id':listid[i],'name':listname[i],
                          'url':listurl[i],'project':listkey[i],
                          'coneent':listconeent[i],
                          'fangshi':listfangshi[i],
                          'qiwang':listqiwang[i],
                          'model':listmodel[i],'headers':listheaders[i],
                          'pid':listpid[i],'is_data':listis_data[i],
                          'sql':listsql[i],'ziduan':listziduan[i],
                          'yican':listyican[i],'save':listsave[i],
                          'testurl':list_test_url[i]})
        i+=1
    return make_data