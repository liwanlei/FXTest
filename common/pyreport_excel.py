# encoding: utf-8
"""
@author: lileilei
@file: pyreport_excel.py
@time: 2017/6/7 8:47
"""
from xlwt import *
import os
def create(filename,listids,listnames,listproject,
           listconeents,listurls,listfangshis,listqiwangs,listmodel,
           listheaders,listpid,listis_data,listsql,listziduan,listyican,
           listsave,testurl):
    file = Workbook(filename)
    table1 = file.add_sheet('测试详情',cell_overwrite_ok=True)
    table1.write_merge(0,0,0,8,'测试详情')
    for i in range(0, 8):
        table1.col(i).width = 400*20
    table1.write(1,0,'用例ID')
    table1.write(1,1,'用例名字')
    table1.write(1,2,'项目')
    table1.write(1,3,'请求内容')
    table1.write(1,4,'url')
    table1.write(1,5,'请求方式')
    table1.write(1,6,'预期')
    table1.write(1,7,'model')
    table1.write(1,8,'headers')
    table1.write(1,9,'pid')
    table1.write(1,10,'is_data')
    table1.write(1,11,'sql')
    table1.write(1,12,'ziduan')
    table1.write(1,13,'yican')
    table1.write(1,14,'save')
    table1.write(1,15,'test')
    for i in range(len(listids)):
        print(testurl)
        table1.write(i+1, 0, listids[i])
        table1.write(i+1, 1, listnames[i])
        table1.write(i+1, 2, listproject[i])
        table1.write(i+1, 3, listconeents[i])
        table1.write(i+1, 4, listurls[i])
        table1.write(i+1, 5, listfangshis[i])
        table1.write(i+1, 6, listqiwangs[i])
        table1.write(i+1,7,listmodel[i])
        table1.write(i+1,8,listheaders[i])
        table1.write(i+1,9,listpid[i])
        table1.write(i+1,10,listis_data[i])
        table1.write(i+1,11,listsql[i])
        table1.write(i+1,12,listziduan[i])
        table1.write(i+1,13,listyican[i])
        table1.write(i+1,14, listsave[i])
        table1.write(i+1,15,testurl)
    file.save(filename)