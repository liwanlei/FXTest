# -*- coding: utf-8 -*-
# @Time    : 2018/10/13 17:03
# @Author  : lileilei
# @File    : testresukt.py
#测试结果写入
from  common.lognew import logger
file_path='result.txt'
@logger('写入测试结果')
def save_result_data(data):
    with open(file_path,'a+') as f:
        f.writelines(data+'\n')
@logger('解析测试结果')
def pase_resukt(filepath):
    listjson=[]
    listurl=[]
    listassert=[]
    listconent=[]
    listproject=[]
    listfangshi=[]
    passnum=0
    listid=[]
    listname=[]
    list_result=[]
    fail=0
    exception=0
    weizhi=0
    listheaders=[]
    with open(r'%s'%filepath,'r+') as f:
        result=f.readlines()
        for item in result:
            result=item.split('$')
            listjson.append(result[0])
            listid.append(result[1])
            listurl.append(result[3])
            listconent.append(result[5])
            listfangshi.append(result[4])
            listproject.append(result[2])
            listheaders.append(result[6])
            listassert.append(result[7])
            if result[8]=='pass':
                passnum+=1
                list_result.append('pass')
            elif result[1]=='fail':
                fail+=1
                list_result.append('fail')
            elif result[1]=='exception':
                exception+=1
                list_result.append('exception')
            else:
                weizhi+=1
                list_result.append('未知错误')
    return  listjson,listurl,listassert,listconent,\
            listproject,listfangshi,passnum,listid,\
            listname,list_result,fail,exception,weizhi,listheaders