# -*- coding: utf-8 -*-
# @Time    : 2018/10/13 20:06
# @Author  : lileilei
# @File    : case.py
from common.requ_case import Api
from  common.get_excel import makedata
from  common.lognew import LOG
import ddt,unittest,os
from common.panduan import assert_in,pare_result_mysql
from app.models import *
from common.testresukt import save_result_data
from common.mysqldatabasecur import cursemsql,excemysql
path = os.getcwd() + '\\app\\upload\\case.xlsx'
data_test=makedata()
@ddt.ddt
class MyTest(unittest.TestCase):
    def setUp(self):
        LOG.info('测试用例开始执行')
    def tearDown(self):
        LOG.info('测试用例执行完毕')
    @ddt.data(*data_test)
    def test_api(self,data_test):
        urltest = Interfacehuan.query.filter_by(url=data_test['testurl']).first()
        testcase=InterfaceTest.query.filter_by(id=data_test['id']).first()
        try:
            yuanlai = eval(data_test['parm'])
            mysql_result = []
            if data_test['pid'] != "None":
                try:
                    test_case = TestcaseResult.query.filter_by(case_id=int(data_test['pid'])).first()
                    data = test_case.result
                    if data:
                        try:
                            huoqudata = eval(data)[data_test['getattr_p']]
                            yuanlai.update({data_test['getattr_p']: huoqudata})
                            if data_test['isdata']!='' or data_test['isdata']!='None' :
                                if urltest is None:
                                    save_result_data('测试环境不存在'+'$'+data_test['id']+'$'+data_test['project']+'$'+data_test['url']+'$'+data_test['fangshi']+'$'+data_test['coneent']+'$'+data_test['headers']+'$'+data_test['qiwang']+'$'+'fail')
                                if testcase.chaxunshujuku is None or testcase.databaseziduan is None:
                                    save_result_data( '用例找不到查询数据库或者断言参数' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                                if urltest.database is None:
                                    save_result_data( '数据库没有配置' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' + data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                                if urltest.dbport is None or urltest.dbhost is None:
                                    save_result_data('检查数据库的地址和端口' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                                if urltest.databaseuser is None or urltest.databasepassword is None:
                                    save_result_data('数据库登录账户没有找到' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                                conncts = cursemsql(host=urltest.dbhost, port=urltest.dbport,user=urltest.databaseuser,
                                                    password=urltest.databasepassword, database=urltest.database)
                                if conncts['code'] == 0:
                                    save_result_data( '链接数据库异常' + '$' + str(data_test['id'] )+ '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                                else:
                                    result_myql = excemysql(conne=conncts['conne'], Sqlmy=data_test['sql'])
                                    if result_myql['code'] == 0:
                                        save_result_data(conncts['e'] + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test[ 'coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                                    mysql_result = result_myql['result']
                            else:
                                try:
                                    api = Api(url=data_test['url'], fangshi=data_test['fangshi'], params=yuanlai,
                                              headers=data_test['headers'])
                                    apijson = api.getJson()
                                    if data_test['save'] !='':
                                        new_case = TestcaseResult(result=str(apijson), case_id=testcase.id)
                                        db.session.add(new_case)
                                        try:
                                            db.session.commit()
                                        except Exception as e:
                                            db.session.rollback()
                                            LOG.info('用例：%s保存测试结果失败!原因：%s' % (data_test['id'], e))
                                    LOG.info(u'测试的:接口地址：%s,请求头：%s,参数:%s,实际返回:%s,预期:%s' % (
                                        data_test['url'], data_test['headers'], data_test['coneent'], apijson,
                                        data_test['qiwang']))
                                    come = assert_in(data_test['qiwang'], apijson)
                                    return_mysql = pare_result_mysql(mysqlresult=mysql_result, return_result=come,
                                                                     paseziduan=data_test['ziduan'])
                                    if come == 'pass' and return_mysql['result'] == 'pass':
                                        save_result_data(apijson + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'pass')
                                    elif come == 'fail' or return_mysql['result'] == 'fail':
                                        save_result_data(apijson + '$' + str(data_test['id'])+ '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                                    elif come == '预期不存在':
                                        save_result_data(apijson + '$' + str(data_test['id'])+ '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + '预期不存在')
                                    elif '异常' in come or return_mysql['code'] == 1:
                                        save_result_data(apijson + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                                    else:
                                        save_result_data(apijson + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + '未知错误')
                                except Exception as e:
                                    LOG.info_log('用例：%s执行失败!原因：%s' % (data_test['id'], e))
                                    save_result_data( e + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                        except Exception as e:
                            LOG.error_log('用例：%s 提出依赖数据出错！原因：%s' % (data_test['id'], e))
                            save_result_data(str('获取依赖的字段异常，%s' % e) + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' + data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                    else:
                        LOG.info_log('用例：%s接口依赖结果没有保存!' % data_test['id'])
                        save_result_data('依赖的测试结果没有保存' + '$' + data_test['id'] + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                except Exception as e:
                    LOG.info_log('用例：%s 测试出错了' % e)
                    save_result_data(str('测试异常，原因：%s' % e) + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
            else:
                if data_test['is_database'] !='None' or data_test['is_database'] !='':
                    if self.urltest is None:
                        save_result_data('测试环境不存在' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                    if testcase.chaxunshujuku is None or testcase.databaseziduan is None:
                        save_result_data( '用例找不到查询数据库或者断言参数' + '$' + str(data_test['id'])+ '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                    if urltest.database is None:
                        save_result_data('数据库没有配置' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' + data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                    if urltest.dbport is None or self.urltest.dbhost is None:
                        save_result_data( '检查数据库的地址和端口' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' + data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                    if urltest.databaseuser is None or urltest.databasepassword is None:
                        save_result_data('数据库登录账户没有找到' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' + data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                    conncts = cursemsql(host=urltest.dbhost, port=urltest.dbport,user=urltest.databaseuser, password=urltest.databasepassword,database=urltest.database)
                    if conncts['code'] == 0:
                        save_result_data('链接数据库异常' + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                    else:
                        result_myql = excemysql(conne=conncts['conne'], Sqlmy=data_test['sql'])
                        if result_myql['code'] == 0:
                            save_result_data(str(conncts['e']) + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                        mysql_result = result_myql['result']
                else:
                    try:
                        api = Api(url=data_test['url'], fangshi=data_test['fangshi'], params=yuanlai,
                                  headers=data_test['headers'])
                        apijson = api.getJson()
                        if data_test['save']!='' or data_test['save'] !='None':
                            new_case = TestcaseResult(result=str(apijson), case_id=testcase.id)
                            db.session.add(new_case)
                            try:
                                db.session.commit()
                            except Exception as e:
                                db.session.rollback()
                                LOG.info('用例：%s保存测试结果失败!原因：%s' %(data_test['id'], e))
                        LOG.info_log(u'测试的:接口地址：%s,请求头：%s,参数:%s,实际返回:%s,预期:%s' % (data_test['url'], data_test['headers'], data_test['coneent'], apijson,data_test['qiwang']))
                        come = assert_in(data_test['qiwang'], apijson)
                        return_mysql = pare_result_mysql(mysqlresult=mysql_result, return_result=come,paseziduan=data_test['ziduan'])
                        if come == 'pass' and return_mysql['result'] == 'pass':
                            save_result_data(str(apijson) + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'pass')
                        elif come == 'fail' or return_mysql['result'] == 'fail':
                            save_result_data(str(apijson) + '$' + str(data_test['id'])+ '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'fail')
                        elif come == '预期不存在':
                            save_result_data(str(apijson) + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + '预期不存在')
                        elif '异常' in come or return_mysql['code'] == 1:
                            save_result_data(str(apijson)+str(return_mysql['result']) + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' + data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
                        else:
                            save_result_data(str(apijson)  + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + '未知错误')
                    except Exception as e:
                        LOG.info('用例：%s执行失败!原因：%s' % (data_test['id'], e))
                        save_result_data(str(e) + '$' + str(data_test['id']) + '$' + data_test['project'] + '$' + data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')
        except Exception as e:
            LOG.info('用例：%s转化参数！原因：%s' % (data_test['id'], e))
            save_result_data(str(e) + '$' + str(data_test['id']) + '$' + data_test[ 'project'] + '$' +data_test['url'] + '$' + data_test['fangshi'] + '$' + data_test['coneent'] + '$' + data_test['headers'] + '$' + data_test['qiwang'] + '$' + 'Exception')